import React from 'react';

import Match from './Match'

function Round(props) {
    return (
        <div className="Round">
            <h2>Round {props.n}</h2>
            <div class="Matches">
                <Match teamA="france" teamB="wales" date="1 Feb 20:00" />
                <Match teamA="scotland" teamB="italy" date="2 Feb 14:15" />
                <Match teamA="ireland" teamB="england" date="2 Feb 16:45" />
            </div>
        </div>
    )
}

export default Round;
