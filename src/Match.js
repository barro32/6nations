import React from 'react';

function Match(props) {
    return (
        <div className="Match">
            <h2>
                <span className="teams">{props.teamA} V {props.teamB}</span>
                <span>{props.date}</span>
            </h2>
            <div>
                <h3>Result</h3>
                <label>
                    {props.teamA}
                    <input type="radio" name="win-{props.teamA}-{props.teamB}" value="{props.teamA}" />
                </label>
                <label>
                    {props.teamB}
                    <input type="radio" name="win-{props.teamA}-{props.teamB}" value="{props.teamB}" />
                </label>
                <label>
                    Draw
                    <input type="radio" name="win-{props.teamA}-{props.teamB}" value="draw" />
                </label>
            </div>
            <div>
                <h3>Spread</h3>
                <label>
                    0 - 5
                    <input type="radio" name="spread-{props.teamA}-{props.teamB}" value="0:5" />
                </label>
                <label>
                    6 - 10
                    <input type="radio" name="spread-{props.teamA}-{props.teamB}" value="6:10" />
                </label>
                <label>
                    11 - 20
                    <input type="radio" name="spread-{props.teamA}-{props.teamB}" value="11:20" />
                </label>
                <label>
                    21+
                    <input type="radio" name="spread-{props.teamA}-{props.teamB}" value="21+" />
                </label>
            </div>
            <div>
                <h3>Total tries</h3>
                <label>
                    <input type="number" />
                </label>
            </div>
        </div>
    )
}

export default Match;
