import React, { Component } from 'react';
import './App.css';

import Round from './Round'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>6 Nations</h1>
        </header>
        <div className="Rounds">
          <Round n="1" />
          <Round n="2" />
          <Round n="3" />
          <Round n="4" />
          <Round n="5" />
        </div>
        <div className="Footer">&copy; barrocorp 2019</div>
      </div>
    );
  }
}

export default App;
